package com.farmacia.apirest.ws.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.farmacia.apirest.ws.model.Usuario;

@Repository
public interface FarmaciaRepository extends JpaRepository<Usuario, Long> {

}
